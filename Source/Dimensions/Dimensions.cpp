// Copyright Epic Games, Inc. All Rights Reserved.

#include "Dimensions.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Dimensions, "Dimensions" );
