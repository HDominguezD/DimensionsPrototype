// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Dimensions/Core/DimensionsEnums.h"
#include "DimensionsGameManager.generated.h"

UCLASS()
class DIMENSIONS_API ADimensionsGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADimensionsGameManager();

	/** Camera Manager BP*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions|Camera", meta = (DisplayName = "Camera Manager BP"))
	    TSubclassOf<class UDimensionsCameraManager> CameraManagerBP;

	/** World location to move player and Dimensionable objects when changing to front game mode
     * Example:
     *  -10.000 and frontMode: The player will be move to position (-10.000, Y, Z) when changing to frontMode.
     */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions", meta = (DisplayName = "Front Mode World Locations"))
		float FrontWorldLocations;

	/** World location to move player and Dimensionable objects when changing to top game mode
	 * Example:
	 *  10.000 and topMode: The player will be move to position (X, Y, 10.000) when changing to topMode.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions", meta = (DisplayName = "Top Mode World Locations"))
		float TopWorldLocations;

	/** Camera Manager */
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Camera", meta = (DisplayName = "Camera Manager"))
		UDimensionsCameraManager* CameraManager;

	/**Current Character*/
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions", meta = (DisplayName = "Player Character"))
		class ADimensionsCharacter* MainCharacter;

	/**Dimensionable Actors*/
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions", meta = (DisplayName = "DimensionableActors"))
	    TArray<class ADimensionable*> DimensionableActors;

	/**Current Game Mode*/
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions", meta = (DisplayName = "Current Game Mode"))
	    EGameMode CurrentGameMode;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/**Change camera to use a new configuration*/
	UFUNCTION(BlueprintCallable)
		void ChangeGameMode(EGameMode NewGameMode);

};
