// Fill out your copyright notice in the Description page of Project Settings.


#include "DimensionsGameManager.h"
#include "DimensionsCameraManager.h"
#include "Dimensions/Characters/DimensionsCharacter.h"
#include "Dimensions/Core/Dimensionable.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ADimensionsGameManager::ADimensionsGameManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADimensionsGameManager::BeginPlay()
{
	Super::BeginPlay();

	MainCharacter = Cast<ADimensionsCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	//Initialize Camera Manager
	if(CameraManagerBP)
	{
		CameraManager = Cast<UDimensionsCameraManager>(CameraManagerBP->GetDefaultObject());
		CameraManager->MainCharacter = MainCharacter;
	}

	//Initialize Dimensionable Objects
	TArray<AActor*> WorldActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ADimensionable::StaticClass(), WorldActors);
	for(AActor* WorldActor : WorldActors)
	{
		ADimensionable* DimensionableActor = Cast<ADimensionable>(WorldActor);
		if(DimensionableActor)
		{
			DimensionableActor->InitGameModes(FrontWorldLocations, TopWorldLocations);
			DimensionableActors.Add(DimensionableActor);
		}
	}

	//Initialize Player
	MainCharacter->InitGameModes(FrontWorldLocations, TopWorldLocations);

	ChangeGameMode(EGameMode::ISOMETRIC);
}

// Called every frame
void ADimensionsGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADimensionsGameManager::ChangeGameMode(EGameMode NewGameMode)
{
	if(!MainCharacter->GetCharacterMovement()->IsFalling() && NewGameMode != CurrentGameMode)
	{
		//Change mode in Camera Manager
		if (CameraManager)
		{
			CameraManager->ChangeConfiguration(NewGameMode);
		}

		//Change mode in Dimensionable Objects
		for (auto DimensionableActor : DimensionableActors)
		{
			DimensionableActor->ChangeGameMode(NewGameMode);
		}

		//Change mode in Player
		MainCharacter->ChangeGameMode(NewGameMode);

		CurrentGameMode = NewGameMode;
	}
}

