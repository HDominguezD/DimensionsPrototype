// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Dimensions/Core/DimensionsEnums.h"
#include "DimensionsCameraManager.generated.h"

USTRUCT(BlueprintType)
struct DIMENSIONS_API FCameraConfig
{
    GENERATED_BODY()

    /** Camera Transform */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|SpringArm", meta = (DisplayName = "Transform"))
        FTransform Transform;

    /** Distance from player to camera */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|SpringArm", meta = (DisplayName = "Arm lenght"))
        float ArmLength;

    /** Focused target offset*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|SpringArm", meta = (DisplayName = "Target Offset"))
        FVector TargetOffset;

    /** Camera Projection mode */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|CameraComponent", meta = (DisplayName = "Projection Mode"))
        EProjectionMode ProjectionMode;

    /** Projection widht when the camera is on Ortographic mode */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|CameraComponent", meta = (DisplayName = "Ortho Width", EditCondition = "ProjectionMode == EProjectionMode::ORTHOGRAPHIC"))
        float OrthoWidth;

    /** Min distance the camera will render when the camera is on Ortographic mode */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|CameraComponent", meta = (DisplayName = "Ortho Near Clip Plane", EditCondition = "ProjectionMode == EProjectionMode::ORTHOGRAPHIC"))
        float OrthoNearClipPlane;

    /** Max distance the camera will render when the camera is on Ortographic mode */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|CameraComponent", meta = (DisplayName = "Ortho Far Clip Plane", EditCondition = "ProjectionMode == EProjectionMode::ORTHOGRAPHIC"))
        float OrthoFarClipPlane;

    /**Camera view angle when camera is on Perspective mode */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Configuration|CameraComponent", meta = (DisplayName = "Field Of View", EditCondition = "ProjectionMode == EProjectionMode::PERSPECTIVE"))
        float FieldOfView;
};

/**
 *
 */
UCLASS()
class DIMENSIONS_API UDimensionsCameraManager : public UObject
{
    GENERATED_BODY()

public:
    UDimensionsCameraManager();

    /** Camera change blend time. */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (DisplayName = "Blend Time"))
        float BlendTime;

    /**Camera Configuration to use when changing to 3D mode*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (DisplayName = "Isometric Configuration"))
        FCameraConfig IsometricConfig;
    /**Camera Configuration to use when changing to front 2D mode*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (DisplayName = "Front Configuration"))
        FCameraConfig FrontConfig;
    /**Camera Configuration to use when changing to top 2D mode*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (DisplayName = "Top Configuration"))
        FCameraConfig TopConfig;

    /**Current Camera configuration*/
    UPROPERTY(BlueprintReadOnly, Category = "Camera", meta = (DisplayName = "Current Configuration"))
        FCameraConfig CurrentConfig;

    /**Current Character*/
    UPROPERTY(BlueprintReadOnly, Category = "Camera", meta = (DisplayName = "Current Character"))
        class ADimensionsCharacter* MainCharacter;

    /**Change camera to use a new configuration*/
    UFUNCTION(BlueprintCallable)
        void ChangeConfiguration(EGameMode NewGameMode);
};

