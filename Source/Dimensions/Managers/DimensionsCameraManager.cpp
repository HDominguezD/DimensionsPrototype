// Fill out your copyright notice in the Description page of Project Settings.


#include "DimensionsCameraManager.h"
#include "Camera/CameraActor.h"
#include "Dimensions/Characters/DimensionsCharacter.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
UDimensionsCameraManager::UDimensionsCameraManager()
{

}

void UDimensionsCameraManager::ChangeConfiguration(EGameMode NewGameMode)
{
    if (MainCharacter->GetCameraComponent())
    {
        switch (NewGameMode)
        {
            case EGameMode::ISOMETRIC:
            {
                CurrentConfig = IsometricConfig;
            }
            break;
            case EGameMode::FRONT:
            {
                CurrentConfig = FrontConfig;

            }
            break;
            case EGameMode::TOP:
            {
                CurrentConfig = TopConfig;

            }
            break;
        }

        //TO DO: Interpolate transform in time
        MainCharacter->GetCameraBoom()->TargetArmLength = CurrentConfig.ArmLength;
        MainCharacter->GetCameraBoom()->SetRelativeTransform(CurrentConfig.Transform);
        MainCharacter->GetCameraBoom()->TargetOffset = CurrentConfig.TargetOffset;

        switch (CurrentConfig.ProjectionMode)
        {
            case EProjectionMode::ORTHOGRAPHIC:
            {
                MainCharacter->GetCameraComponent()->ProjectionMode = ECameraProjectionMode::Orthographic;
                MainCharacter->GetCameraComponent()->SetOrthoNearClipPlane(CurrentConfig.OrthoNearClipPlane);
                MainCharacter->GetCameraComponent()->SetOrthoFarClipPlane(CurrentConfig.OrthoFarClipPlane);
                MainCharacter->GetCameraComponent()->SetOrthoWidth(CurrentConfig.OrthoWidth);
            }
            break;
            case EProjectionMode::PERSPECTIVE:
            {
                MainCharacter->GetCameraComponent()->ProjectionMode = ECameraProjectionMode::Perspective;
                MainCharacter->GetCameraComponent()->SetFieldOfView(CurrentConfig.FieldOfView);
            }
            break;
        }
    }
}