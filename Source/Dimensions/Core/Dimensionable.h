// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DimensionsEnums.h"
#include "Dimensionable.generated.h"

USTRUCT(BlueprintType)
struct DIMENSIONS_API FDimensionsTrigger
{
    GENERATED_BODY()

    /** Trigger */
    UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Trigger", meta = (DisplayName = "Trigger"))
        UShapeComponent* ShapeComponent;
};

UCLASS(BlueprintType)
class DIMENSIONS_API ADimensionable : public AActor
{
	GENERATED_BODY()

public:

    /** Triggers used to mark object limits on front mode*/
    UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Trigger", meta = (DisplayName = "Front Triggers"))
        TArray<FDimensionsTrigger> FrontTriggers;

    /** Triggers used to mark object limits on top mode*/
    UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Trigger", meta = (DisplayName = "Top Triggers"))
        TArray<FDimensionsTrigger> TopTriggers;

    /** Current game mode*/
    UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Trigger", meta = (DisplayName = "Current Game Mode"))
    EGameMode CurrentGameMode;

private:
    /** Triggers used to mark object limits on top mode*/
    AActor* ThisActor;

    float FrontWorldLocations;
    float TopWorldLocations;

public:
    void InitGameModes(float NewFrontWorldLocations, float NewTopWorldLocations);
    void InitGameModeTriggers(EGameMode GameMode);
    void ChangeGameMode(EGameMode GameMode);

};
