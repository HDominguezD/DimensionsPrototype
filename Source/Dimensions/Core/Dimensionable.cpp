// Fill out your copyright notice in the Description page of Project Settings.


#include "Dimensionable.h"
#include "Components/ActorComponent.h"
#include "Components/ShapeComponent.h"
#include "Sections/MovieSceneParticleSection.h"

void ADimensionable::InitGameModes(float NewFrontWorldLocations, float NewTopWorldLocations)
{
    ThisActor = Cast<AActor>(this);
    FrontWorldLocations = NewFrontWorldLocations;
    TopWorldLocations = NewTopWorldLocations;

    if(ThisActor)
    {
        InitGameModeTriggers(EGameMode::FRONT);
        InitGameModeTriggers(EGameMode::TOP);
    }
}

void ADimensionable::InitGameModeTriggers(EGameMode GameMode)
{
    switch (GameMode)
    {
        case EGameMode::FRONT:
        {
            TArray<UActorComponent*> TaggedSceneComponents = ThisActor->GetComponentsByTag(UActorComponent::StaticClass(), "FrontScene");
            for (auto Component : TaggedSceneComponents)
            {
                UShapeComponent* ShapeComponent = Cast<UShapeComponent>(Component);
                if (ShapeComponent)
                {
                    FDimensionsTrigger DimensionsTrigger;
                    DimensionsTrigger.ShapeComponent = ShapeComponent;
                    DimensionsTrigger.ShapeComponent->SetWorldLocation(FVector(FrontWorldLocations, DimensionsTrigger.ShapeComponent->GetComponentLocation().Y, DimensionsTrigger.ShapeComponent->GetComponentLocation().Z)); //Move trigger to game Mode Location
                    FrontTriggers.Add(DimensionsTrigger);
                }
            }
        }
        break;
        case EGameMode::TOP:
        {
            TArray<UActorComponent*> TaggedSceneComponents = ThisActor->GetComponentsByTag(UActorComponent::StaticClass(), "TopScene");
            for (auto Component : TaggedSceneComponents)
            {
                UShapeComponent* ShapeComponent = Cast<UShapeComponent>(Component);
                if (ShapeComponent)
                {
                    FDimensionsTrigger DimensionsTrigger;
                    DimensionsTrigger.ShapeComponent = ShapeComponent;
                    DimensionsTrigger.ShapeComponent->SetWorldLocation(FVector(DimensionsTrigger.ShapeComponent->GetComponentLocation().X, DimensionsTrigger.ShapeComponent->GetComponentLocation().Y, TopWorldLocations)); //Move trigger to game Mode Location
                    TopTriggers.Add(DimensionsTrigger);
                }
            }
        }
        break;
        case EGameMode::ISOMETRIC:
        {
            //No value is stored because Isometric is object default collision
            return;
        }
        break;
    }
}

void ADimensionable::ChangeGameMode(EGameMode GameMode)
{

    //Activate Game Mode collisions
    switch (GameMode)
    {
        case EGameMode::FRONT:
        {
            for(auto Trigger : FrontTriggers) //Activate front triggers
            {
                Trigger.ShapeComponent->Activate();
            }

            if(CurrentGameMode == EGameMode::TOP) //Deactivate top triggers
            {
                for (auto Trigger : TopTriggers)
                {
                    Trigger.ShapeComponent->Deactivate();
                }
            }
        }
        break;
        case EGameMode::TOP:
        {
            for (auto Trigger : TopTriggers) //Activate front triggers
            {
                Trigger.ShapeComponent->Activate();
            }

            if (CurrentGameMode == EGameMode::FRONT) //Deactivate Front triggers
            {
                for (auto Trigger : FrontTriggers)
                {
                    Trigger.ShapeComponent->Deactivate();
                }
            }
        }
        break;
        case EGameMode::ISOMETRIC:
        {
            //Use default Collision
            if (CurrentGameMode == EGameMode::TOP) //Deactivate top triggers
            {
                for (auto Trigger : TopTriggers)
                {
                    Trigger.ShapeComponent->Deactivate();
                }
            }
            else if (CurrentGameMode == EGameMode::FRONT) //Deactivate Front triggers
            {
                for (auto Trigger : FrontTriggers)
                {
                    Trigger.ShapeComponent->Deactivate();
                }
            }
        }
        break;
    }

    CurrentGameMode = GameMode;
}
