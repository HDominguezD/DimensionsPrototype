// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EGameMode : uint8
{
	ISOMETRIC = 0 UMETA(DisplayName = "ISOMETRIC"),
	FRONT = 1  UMETA(DisplayName = "FRONT"),
	TOP = 2 UMETA(DisplayName = "TOP")
};

UENUM(BlueprintType)
enum class EProjectionMode : uint8
{
	ORTHOGRAPHIC = 0 UMETA(DisplayName = "ORTHOGRAPHIC"),
	PERSPECTIVE = 1  UMETA(DisplayName = "PERSPECTIVE")
};