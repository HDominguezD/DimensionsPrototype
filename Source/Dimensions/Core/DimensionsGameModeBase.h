// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DimensionsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIMENSIONS_API ADimensionsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
