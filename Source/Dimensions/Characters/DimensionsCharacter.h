// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Dimensions/Core/DimensionsEnums.h"
#include "DimensionsCharacter.generated.h"

USTRUCT(BlueprintType)
struct DIMENSIONS_API FDimensionsFloorTrigger
{
	GENERATED_BODY()

	/** Trigger */
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions|Trigger", meta = (DisplayName = "Trigger"))
	UShapeComponent* ShapeComponent;

	/** Save position when changing from Isometric mode */
	FVector OriginalFloorTriggerLocation;
};

UCLASS()
class DIMENSIONS_API ADimensionsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADimensionsCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/**Current Game Mode*/
	UPROPERTY(BlueprintReadOnly, Category = "Dimensions", meta = (DisplayName = "Current Game Mode"))
		EGameMode CurrentGameMode;

public:
	UFUNCTION(BlueprintCallable)
		class UCameraComponent* GetCameraComponent();

	UFUNCTION(BlueprintCallable)
		class USpringArmComponent* GetCameraBoom();

	void InitGameModes(float NewFrontWorldLocations, float NewTopWorldLocations);

	void ChangeGameMode(EGameMode NewGameMode);

	void UpdateNext2DFloor(EGameMode NextGameMode);

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;

	void Tick(float DeltaSeconds) override;

private:
	class ADimensionsGameManager* DimensionsGameManager;
	DECLARE_DELEGATE_OneParam(FChangeConfigDelegate,  EGameMode);

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    UCameraComponent* FollowCamera;

	float FrontWorldLocations;
	float TopWorldLocations;

	/** Save location when changing between game modes */
	FVector LastCharacterLocation;

	TArray< FDimensionsFloorTrigger> LastDimensionsFloorTriggers;
	AActor* LastFloorActor;

};
