// Fill out your copyright notice in the Description page of Project Settings.


#include "DimensionsCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"
#include "Dimensions/Dimensions.h"
#include "GameFramework/SpringArmComponent.h"
#include "Dimensions/Managers/DimensionsGameManager.h"
#include "Dimensions/Dimensions.h"

ADimensionsCharacter::ADimensionsCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 3000.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ADimensionsCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	DimensionsGameManager = Cast<ADimensionsGameManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ADimensionsGameManager::StaticClass()));
	if(DimensionsGameManager)
	{
		PlayerInputComponent->BindAction<FChangeConfigDelegate>("ChangeIsometric", IE_Released, DimensionsGameManager, &ADimensionsGameManager::ChangeGameMode, EGameMode::ISOMETRIC);
		PlayerInputComponent->BindAction<FChangeConfigDelegate>("ChangeFront", IE_Released, DimensionsGameManager, &ADimensionsGameManager::ChangeGameMode, EGameMode::FRONT);
		PlayerInputComponent->BindAction<FChangeConfigDelegate>("ChangeTop", IE_Released, DimensionsGameManager, &ADimensionsGameManager::ChangeGameMode, EGameMode::TOP);
	}

	PlayerInputComponent->BindAxis("MoveForward", this, &ADimensionsCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ADimensionsCharacter::MoveRight);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ADimensionsCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ADimensionsCharacter::TouchStopped);
}

void ADimensionsCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ADimensionsCharacter::Tick(float DeltaSeconds)
{

}

void ADimensionsCharacter::InitGameModes(float NewFrontWorldLocations, float NewTopWorldLocations)
{
	FrontWorldLocations = NewFrontWorldLocations;
	TopWorldLocations = NewTopWorldLocations;
}


void ADimensionsCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ADimensionsCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

UCameraComponent* ADimensionsCharacter::GetCameraComponent()
{
	return FollowCamera;
}

USpringArmComponent* ADimensionsCharacter::GetCameraBoom()
{
	return CameraBoom;
}

void ADimensionsCharacter::ChangeGameMode(EGameMode NewGameMode)
{
	//Find Current 3D floor
	FFindFloorResult FindFloorResult;
	GetCharacterMovement()->ComputeFloorDist(GetActorLocation(), GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 20.f, FindFloorResult, 10.f);

	switch (NewGameMode)
	{
	    case EGameMode::FRONT:
	    {
		    //Save last X Position
		    LastCharacterLocation.X = GetActorLocation().X;

		    //Update Positions
		    if (CurrentGameMode == EGameMode::ISOMETRIC)
		    {
				UpdateNext2DFloor(NewGameMode);

			    TeleportTo(FVector(FrontWorldLocations, GetActorLocation().Y, GetActorLocation().Z), GetActorRotation());
		    }
		    else if (CurrentGameMode == EGameMode::TOP)
		    {
			    //Move floor colliders to be under player position
			    for (auto LastDimensionsFloorTrigger : LastDimensionsFloorTriggers)
			    {
				    LastDimensionsFloorTrigger.ShapeComponent->SetWorldLocation(FVector(FrontWorldLocations, LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.Y, LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.Z)); //Move trigger to game Mode Location
			    }

			    TeleportTo(FVector(FrontWorldLocations, GetActorLocation().Y, FindFloorResult.HitResult.GetComponent()->GetComponentLocation().Z + GetCapsuleComponent()->GetScaledCapsuleHalfHeight()), GetActorRotation());
		    }

		    //Lock Movement in X axis
		    GetMovementComponent()->SetPlaneConstraintEnabled(true);
		    GetMovementComponent()->SetPlaneConstraintAxisSetting(EPlaneConstraintAxisSetting::X);

		    //Allow jump
		    GetMovementComponent()->SetJumpAllowed(true);

	    }
	    break;
	    case EGameMode::TOP:
	    {
			//Update Postions
		    if (CurrentGameMode == EGameMode::ISOMETRIC)
		    {
				UpdateNext2DFloor(NewGameMode);

			    TeleportTo(FVector(GetActorLocation().X, GetActorLocation().Y, TopWorldLocations), GetActorRotation());
		    }
		    else if (CurrentGameMode == EGameMode::FRONT)
		    {
			    //Move floor colliders to be under player position
			    for (auto LastDimensionsFloorTrigger : LastDimensionsFloorTriggers)
			    {
				    float DistanceFromFloor = GetActorLocation().Z - LastDimensionsFloorTrigger.ShapeComponent->GetComponentLocation().Z;
				    LastDimensionsFloorTrigger.ShapeComponent->SetWorldLocation(FVector(LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.X, LastDimensionsFloorTrigger.ShapeComponent->GetComponentLocation().Y, TopWorldLocations - DistanceFromFloor)); //Move trigger to game Mode Location
			    }

			    float NewXPosition = LastCharacterLocation.X;

			    AActor* CurrentFloorActor = FindFloorResult.HitResult.GetActor();
			    if (LastFloorActor != CurrentFloorActor) // The player is in a new platform, set him in the center of the platform
			    {
				    NewXPosition = CurrentFloorActor->GetActorLocation().X;
			    }

			    TeleportTo(FVector(NewXPosition, GetActorLocation().Y, TopWorldLocations), GetActorRotation());
		    }

		    //Unlock movement in X Axis
		    GetMovementComponent()->SetPlaneConstraintEnabled(false);
		    //Lock Jump
		    GetMovementComponent()->SetJumpAllowed(false);

	    }
	    break;
	    case EGameMode::ISOMETRIC:
	    {
		    //Move last floor colliders to original position
		    for (auto LastDimensionsFloorTrigger : LastDimensionsFloorTriggers)
		    {
			    LastDimensionsFloorTrigger.ShapeComponent->SetWorldLocation(LastDimensionsFloorTrigger.OriginalFloorTriggerLocation); //Move trigger to game Mode Location
		    }

			AActor* CurrentFloorActor = FindFloorResult.HitResult.GetActor();

		    if(CurrentGameMode == EGameMode::FRONT)
		    {
			    float NewXPosition = LastCharacterLocation.X;

			    
			    if(LastFloorActor != CurrentFloorActor) // The player is in a new platform, set him in the center of the platform
			    {
				    NewXPosition = CurrentFloorActor->GetActorLocation().X;
			    }
			    
			    TeleportTo(FVector(NewXPosition, GetActorLocation().Y, GetActorLocation().Z), GetActorRotation());
		    }
		    else if(CurrentGameMode == EGameMode::TOP)
		    {
			    TeleportTo(FVector(GetActorLocation().X, GetActorLocation().Y, CurrentFloorActor->GetActorLocation().Z + GetCapsuleComponent()->GetScaledCapsuleHalfHeight()), GetActorRotation());
		    }

		    //Unlock Movement in all axis
		    GetMovementComponent()->SetPlaneConstraintEnabled(false);
		    GetMovementComponent()->SetJumpAllowed(true);

	    }
	    break;
	}

	CurrentGameMode = NewGameMode;
}

void ADimensionsCharacter::UpdateNext2DFloor(EGameMode NextGameMode)
{
	FVector NextTraceFloorOffset = FVector::ZeroVector;

	switch(NextGameMode)
	{
	    case EGameMode::FRONT:
		{
			NextTraceFloorOffset = FVector(-GetCameraBoom()->TargetArmLength, 0, 0);
		}
		break;
		case EGameMode::TOP:
		{
			NextTraceFloorOffset = FVector(0, 0, GetCameraBoom()->TargetArmLength);
		}
		break;
		case EGameMode::ISOMETRIC:
	    {
			//Don't update 2D floor
			return;
	    }
		break;
	}

	//Find Current floor to player to move with him
	FFindFloorResult FindFloorResult;
	GetCharacterMovement()->ComputeFloorDist(GetActorLocation(), GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 20.f, FindFloorResult, 10.f);

	//Check if a dimensionable actor is in front of the character
	FHitResult HitResult;
	FCollisionShape Capsule;
	Capsule.SetCapsule(GetCapsuleComponent()->GetScaledCapsuleRadius(), GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);

	GetWorld()->SweepSingleByChannel(HitResult, GetActorLocation() + NextTraceFloorOffset, GetActorLocation(), FQuat::Identity, ECC_Dimensionable, Capsule, Params);

	//Set last floor
	if (HitResult.GetActor())
	{
		LastFloorActor = HitResult.GetActor();
	}
	else
	{
		LastFloorActor = FindFloorResult.HitResult.GetActor();
	}

	//Set Last floor Triggers
	TArray<UActorComponent*> TaggedSceneComponents = LastFloorActor->GetComponentsByTag(UActorComponent::StaticClass(), "IsometricScene");

	LastDimensionsFloorTriggers.Empty();
	for (auto Component : TaggedSceneComponents)
	{
		UShapeComponent* LastFloorShapeComponent = Cast<UShapeComponent>(Component);
		if (LastFloorShapeComponent)
		{
			FDimensionsFloorTrigger LastFloorTrigger;
			LastFloorTrigger.ShapeComponent = LastFloorShapeComponent;
			LastFloorTrigger.OriginalFloorTriggerLocation = LastFloorShapeComponent->GetComponentLocation();

			LastDimensionsFloorTriggers.Add(LastFloorTrigger);
		}
	}

	//Move floor colliders to be under player position
	switch (NextGameMode)
	{
	    case EGameMode::FRONT:
	    {
			for (auto LastDimensionsFloorTrigger : LastDimensionsFloorTriggers)
			{
				LastDimensionsFloorTrigger.ShapeComponent->SetWorldLocation(FVector(FrontWorldLocations, LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.Y, LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.Z)); //Move trigger to game Mode Location
			}
	    }
	    break;
	    case EGameMode::TOP:
	    {
			for (auto LastDimensionsFloorTrigger : LastDimensionsFloorTriggers)
			{
				//Get current distance to floor
				float DistanceFromFloor = 0;

				if (HitResult.GetActor())
				{
					DistanceFromFloor = (HitResult.Location.Z + GetCapsuleComponent()->GetScaledCapsuleHalfHeight()) - LastDimensionsFloorTrigger.ShapeComponent->GetComponentLocation().Z; //Player location if it will be over this platform - Trigger location
				}
				else
				{
					DistanceFromFloor = GetActorLocation().Z - LastDimensionsFloorTrigger.ShapeComponent->GetComponentLocation().Z; //Player location over this platform - Trigger location
				}

				LastDimensionsFloorTrigger.ShapeComponent->SetWorldLocation(FVector(LastDimensionsFloorTrigger.OriginalFloorTriggerLocation.X, LastDimensionsFloorTrigger.ShapeComponent->GetComponentLocation().Y, TopWorldLocations - DistanceFromFloor)); //Move trigger to game Mode Location
			}
	    }
	    break;
	    case EGameMode::ISOMETRIC:
	    {
		    //Don't move 2D floor
		    return;
	    }
	    break;
	}
}

void ADimensionsCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);

		const FRotator Rotation = CameraManager->GetCameraRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ADimensionsCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);

		const FRotator Rotation = CameraManager->GetCameraRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}